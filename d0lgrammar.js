//
// An implementation of L-system grammars of the D0L kind.
//
// D0L-systems are described in [The algorithmic beauty of plants][abop] by 
// Przemyslaw Prusinkiewicz and Aristid Lindenmayer.
//
// A D0L grammar works with a set of replacement rules on a string.
// Given an axiom (initial string, generation 0), successive generations are 
// created by replacing each variable (substring) that matches a production rule. 
//
// Each generation contains an instruction set that is suitable for turtle 
// interpretation. Please refer to `turtle.js` for an explanation of the drawing 
// instructions.
//
// This implementation focuses on applying production rules, it does't enforce
// compliance with an alphabeth and it doesn't enforce validity of the production 
// rules.
//
// [abop]: http://algorithmicbotany.org/papers/abop/abop.pdf
//
function D0LGrammar (axiom, productions) {
    
    this.axiom = axiom;
    this.productions = productions;
    
    this.replacementPattern = this.createReplacementPattern();
}

D0LGrammar.prototype = {
    
    // creates the replacementPattern which is used to apply production rules
    createReplacementPattern : function () {
        var v, variables = [];
        
        // collect the replacement variables (left hand sides of productions)
        for (v in this.productions) {
            // makes sure not to include `prototypical` properties
            if (this.productions.hasOwnProperty(v)) {
                variables.push(v);
            }
        }
        
        // Create a regular expression that matches the production 'left hand sides'.
        // Assumed is that the variables don't need escaping.
        return new RegExp(variables.join("|"), "g");
    },
    
    // generate the successor of `current`
    successor : function (current) {
        var self = this;

        return current.replace(this.replacementPattern, function (m) {
            return self.productions[m];
        });
    },
    
    // Produces generation `n`, where generation 0 is the axiom 
    getGeneration : function (n) {
        var gen = this.axiom;
        
        while (n > 0) {
            gen = this.successor(gen);
            n--;
        }
        
        return gen;
    }
};

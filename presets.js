// A number of fractal drawing specifications.
// Most of these can be found in 'The algorithmic beaty of plants',
// some are variations I discovered by playing with settings.
//
var presets = [
    
    {
        title : "Quadratic Koch Island",
        axiom : "F-F-F-F",
        productions : { 
            "F" : "F+F-F-FF+F+F-F" 
        },
        angle : 90,
        generation : 3
    },
    
    {
        title : "Koch Snowflake",
        axiom : "F--F--F",
        productions : { 
            "F" : "F+F--F+F" 
        },
        angle : 60,
        generation : 4
    },
    
    {
        title : "Islands and Lakes",
        axiom : "F-F-F-F",
        productions : {
            "F" : "F-f+FF-F-FF-Ff-FF+f-FF+F+FF+Ff+FFF",
            "f" : "ffffff"
        },
        angle : 90,
        generation : 2
    },
    
    {
        title : "Islands and Lakes (45° version)",
        axiom : "F-F-F-F-F-F-F-F",
        productions : {
            "F" : "F--f++FF--F--FF--Ff--FF++f--FF++F++FF++Ff++FFF",
            "f" : "ffffff"
        },
        angle : 45,
        generation : 2
    },
    
    {
        title : "Sierpinski arrowhead",
        axiom : "YF",
        productions : {
            "X" : "YF+XF+Y",
            "Y" : "XF-YF-X"
        },
        angle : 60,
        generation : 7
    },
    
    {
        title : "Hexagonal Gosper curve",
        axiom : "XF",
        productions : {
            "X" : "X+YF++YF-FX--FXFX-YF+",
            "Y" : "-FX+YFYF++YF+FX--FX-Y"
        },
        angle : 60,
        generation : 4
    },
    
    {
        title : "2 Hexagonal Gosper curves",
        axiom : "XY",
        productions : {
            "X" : "X+YF++YF-FX--FXFX-YF+",
            "Y" : "-FX+YFYF++YF+FX--FX-Y"
        },
        angle : 60,
        generation : 4
    },
    
    {
        title : "Box-(in-box)*",
        axiom : "F+F+F+F",
        productions : { 
            "F" : "FF+F+F+F+FF" 
        },
        angle : 90,
        generation : 4
    },
    
    {
        title : "X-Overlap",
        axiom : "F++F++F++F",
        productions : { 
            "F" : "FF+++F+F+F+++FF" 
        },
        angle : 45,
        generation : 3
    },
    
    {
        title : "Counter-clockwise",
        axiom : "F++F++F++F",
        productions : { 
            "F" : "F-F+++F++F-F+++++FF" 
        },
        angle : 45,
        generation : 2
    }
];
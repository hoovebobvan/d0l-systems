// The Turtle takes a set of drawing instructions:
//
// 'f'      : move forward
// 'F'      : move forward drawing a line
// '+'      : turn left by `_degrees`
// '-'      : turn right by `_degrees`
// otherwise: ignore
// 
// Given an instuction set it will precalculate the proper line size 
// and canvas position such that the drawing will be centered on the 
// canvas.
//
// On 'drawNext' it will iteratively render the drawing.
//
// Given the framerate a 'linesPerFrame' rate is calculated aiming to
// complete the drawing within 15 seconds.
//
function Turtle (instructions, _degrees, canvasSize, fps) {
    
    angleMode(DEGREES);
    
    // capture instruction set
    this.instructions = instructions;
    // rotation angle
    this.degrees = _degrees;
    // and canvas size
    this.canvasSize = canvasSize;
    // frames per second (might not actually be achieved)
    this.fps = fps;
    
    // keep track of current instruction index
    this.currentIndex = 0;
    // and orientation (degrees) facing 'north'
    this.orientation = 270;    
    
    // To be initialized:

    // current drawing position
    this.pos = createVector(0, 0); 
    // line size
    this.lineSize = null;
    // line segments to draw (F-count)
    this.lineCount = 0; 
    
    // how many lines to draw per frame 
    this.linesPerFrame = this.calculateLinesPerFrame();
    this.calculatePositionAndLineSize();
}

Turtle.prototype = {
    
    calculateLinesPerFrame : function () {
        var rate = 1,
            lineCount = (this.instructions.match(/F/g) || []).length;
        
        // crude way to make sure the drawing doesn't take too long :)
        while (lineCount / this.fps / rate > 15) {
            rate++;
        }
        
        return rate;
    },

    // side effect: sets `this.[pos, lineSize]
    calculatePositionAndLineSize : function () {
    
        // In this function the starting position as well as suitable line
        // size will be calculated. This involves calculating left / right
        // and upper / lower most deviations from the start respecting the
        // given `degrees` for turning instructions
    
        var // make this available in forEach scope
            self = this, 
            // width / height in number of steps (to be calculated)
            stepCountX, stepCountY, 
            // ideal (non-integer) step sizes given the canvas size (to be calculated)
            xStep, yStep,
            // create an object to contain boundaries in step-wise offsets
            bounds = { 
            	left : 0,
            	upper : 0,
            	right : 0,
            	lower : 0
            },
            // keep track of the 'pre-drawing' position
            pos = createVector(0, 0);
            // keep track of the 'pre-drawing' orientation, copying the default
            orientation = this.orientation;
        
        // iterate over the instructions, updating the left / right / upper / lower most variables 
        this.instructions.split('').forEach(function (cmd, i) {
            
            switch (cmd) {
                case "F":
                case "f":
                    // move pre-drawing forward given the current orientation
                    pos.add(p5.Vector.fromAngle(radians(orientation)));
                    break;
                case "+":
                    // change orientation counter clockwise by 'degrees'
                    orientation = self.modulo(orientation - self.degrees, 360);
                    break;
                case "-":                    
                    // change orientation clockwise by 'degrees'
                    orientation = self.modulo(orientation + self.degrees, 360);
                    break;
                // default:
                    // Let's just ignore unknown commands
            }

            // if the current (pre-drawing) position exceeds one of the limits
            // update the corresponding limit
            if (pos.x < bounds.left) bounds.left = pos.x;
            if (pos.y > bounds.lower) bounds.lower = pos.y;
            if (pos.x > bounds.right) bounds.right = pos.x;
            if (pos.y < bounds.upper) bounds.upper = pos.y;
        });

        // Get the width and height of the drawing in terms of number of steps
        stepCountX = bounds.right - bounds.left;
        stepCountY = bounds.lower - bounds.upper;
        
        // Determine the ideal (non-integer) size of a step in each direction
        // respecting the canvas size and allowing for a 5 percent margin on
        // each edge of the drawing
        //
        xStep = this.canvasSize.x / (stepCountX + 0.1 * stepCountX);
        yStep = this.canvasSize.y / (stepCountY + 0.1 * stepCountY);
        
        // now scale back the step size to be the lowest integer multiple
        this.lineSize = floor(min(xStep, yStep));
        
        // bail out if the lineSize is too small
        if (this.lineSize < 1) {
            throw("Can not draw: line size would be smaller than 1")
        }

        // The ideal starting position centers the drawing.
        //
        // Note that the bounds.left and bounds.upper will most likely be negative, so adding 
        // them to half the width / height expressed in steps will give the 'center offset'
        // in steps.
        //
        this.pos.x = floor(canvasSize.x / 2 - (stepCountX / 2 + bounds.left) * this.lineSize);
        this.pos.y = floor(canvasSize.y / 2 - (stepCountY / 2 + bounds.upper) * this.lineSize);
    },
    
    modulo : function (x, divisor) {
        // Using modulo rather than % - keeps the value in the positive range
        // http://wiki.ecmascript.org/doku.php?id=strawman:modulo_operator
        var remainder = x % divisor;
        return remainder >= 0 ? remainder : remainder + divisor;
    },
    
    hasNext : function () {
        return this.currentIndex < this.instructions.length;
    },
    
    _drawNext : function () {
        var cmd, fromPos, forward;
        
        if (this.currentIndex < this.instructions.length) {
            cmd = this.instructions[this.currentIndex];
            
            if (cmd == "f" || cmd == "F") {
                // calculate a 'step forward'  
                forward = (p5.Vector
                    .fromAngle(radians(this.orientation)) // get the direction
                    .setMag(this.lineSize)); // and scale with lineSize
            }
            
            switch (cmd) {
                case "f":
                    // Apply 'move forward'
                    this.pos.add(forward);
                    break;
                    
                case "F":
                    // Apply 'draw forward'
                    fromPos = this.pos.copy();
                    this.pos.add(forward);
                    
                    // Set stroke weight ..
                    strokeWeight(1);
                    // .. color the line based on the orientation ..
                    colorMode(HSB);
                    stroke(map(this.orientation, 0, 360, 0, 210), 100, 100);
                    // .. and draw it.
                    line(fromPos.x, fromPos.y, this.pos.x, this.pos.y);
                    break;
                
                case "+":
                    // Turn left by `degrees`
                    this.orientation = this.modulo(this.orientation - this.degrees, 360);
                    break;
                    
                case "-":
                    // Turn right by `degrees`
                    this.orientation = this.modulo(this.orientation + this.degrees, 360);
                    break;
                    
            }
        }
        
        // advance to next instruction
        this.currentIndex++;
        
        // return true if an actual line was drawn
        return cmd == "F";
    },
    
    drawNext : function () {
        var lineCount = 0;
        
        // Call _drawNext until 'linesPerFrame' is met
        while (this.hasNext() && lineCount < this.linesPerFrame) {
            lineCount += this._drawNext() ? 1 : 0;
        }
    }
};

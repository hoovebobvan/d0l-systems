// Provides some functions to setup user interaction to play around
// with the values of a drawing specification.

// sets up the interaction given the preset list and a delegate to `init drawing`
function setupInteraction (presets, initDrawing) {
    
    // capture the input html elements
    var controls = { 
        axiom : select("#axiom-control"),
        productions : select("#productions-control"),
        generation : select("#generation-control"),
        angle : select("#angle-control"),
        applyChanges : select("#applychanges-control")
    };
    
    // Create a select box for the presets ..
    presetSelect = createSelect().parent("preset-selection");
    
    // .. and fill it with titles and indexes from the list of presets.
    presets.forEach(function (preset, index) {
       presetSelect.option(preset.title, index);
    });
    
    // If the preset changes ..
    presetSelect.changed(function () {
        var preset = presets[presetSelect.value()];
        
        // .. initiate drawing ..
        initDrawing(preset);
        
        // .. and show the settings in the form.
        showPreset(preset, controls);
    });
    
    // If the user clicks apply changes ..
    controls.applyChanges.mouseClicked(function () {
        var spec;
        
        // .. try to construct the (updated) spec and iniitiate drawing.
        try {
            spec = controlValuesToSpec(controls);
            console.log(spec);
            initDrawing(spec);
        }
        catch (error) {
            // Should something go wrong -which is quite possible-
            // open a good old fashioned alert box showing error.
            alert("Error: " + error);
        }
    })
    
    // Make sure the initial preset is shown.
    showPreset(presets[0], controls);
}

// convert production rules to text (to be edited via the form)
function productionsToText (productions) {
    var v, lines = [];
    
    // for each production
    for (v in productions) {
        if (productions.hasOwnProperty(v)) {
            // .. create a line of the form `variable -> replacement`
            lines.push(v + " -> " + productions[v]);
        }
    }
    
    return lines.join("\n");
}

// convert user input to production rules
function textToProductions (t) {
    var // a production rule is a line of the form `variable -> replacement`
        pattern = /([^\s]+)\s*\->\s*([^\n\r]+)/,
        // split the text into lines
        lines = t.split("\n"),
        // and create a variable to contain the productions
        productions = { };
        
    // Read every line.
    lines.forEach(function (l) {
        // Try and match it to the pattern.
        var matches = pattern.exec(l);
        
        // If it matches add a production 
        if (matches) {
            productions[matches[1]] = matches[2];
        }
        // .. and if it doesn't and the line is not empty, raise an error.
        else if (l != "") {
            throw("Invalid production rule: " + l);
        }
    });
    
    return productions;
}

function controlValuesToSpec (controls) {
    // Convert (changed) values in controls back to a new 'spec'.
    // This is likely to fail, it's best to wrap this in a 'try' block.
    return { 
        generation : parseInt(controls.generation.elt.value, 10),
        angle : parseInt(controls.angle.elt.value, 10),
        axiom : controls.axiom.elt.value,
        productions : textToProductions(controls.productions.elt.value)
    };
}

function showPreset (preset, controls) {
    // put preset values in the corresponding input controls
    controls.axiom.elt.value = preset.axiom;
    controls.productions.elt.value = productionsToText(preset.productions);
    controls.generation.elt.value = preset.generation;
    controls.angle.elt.value = preset.angle;
}
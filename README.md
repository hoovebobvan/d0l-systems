# D0L-systems

This demo draws a variety of fractals based on [L‑system][lsys] grammars.
 
Most examples are taken from the book [The algorithmic beauty of plants][abop].

The [p5js][] library is used for drawing on the canvas.

![example fractal](http://courier10pt.com/d0lsystems/example.png)

[Try it here!][10pt].

[lsys]: https://en.wikipedia.org/wiki/L-system
[abop]: http://algorithmicbotany.org/papers/abop/abop.pdf
[p5js]: https://p5js.org/
[10pt]: http://courier10pt.com/d0lsystems/